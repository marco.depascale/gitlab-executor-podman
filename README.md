# gitlab-runner-podman

This a fork of https://gitlab.com/johngoetz/gitlab-executor-podman.git.

This repository is an adaptation of such repo to the targets of PRACE Course on 
Containerization in 2022.

## Registering runners

We are going to register a runner for you blanck project created during the lesson.

In order to register a runner on your host machine change the following variable
```bash
CI_SERVER_URL
REGISTRATION_TOKEN
```
in `register_podman_executor.sh` with the values from your repo, that you can get under
**Settings > CI/CD > Runners > Specific Runners** section on the GitLab webpage.

Save and run `register_podman_executor.sh` to register a Podman runner on your system.

For the original `README.md` look at the `master` branch of this repo.


