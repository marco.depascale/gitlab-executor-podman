#!/usr/bin/env bash
BINDIR=$PWD/scripts
RUNNER_DATA="./"
CI_SERVER_URL="https://gitlab.com/"
REGISTRATION_TOKEN="<REGISTRATION-TOKEN>"

# add volumes for containers to access scripts directory
if [ "$RUNNER_DATA" != "" ] && [ "$REGISTRATION_TOKEN" != "" ]
then
	echo "Registering ... "
	gitlab-runner register \
		--tag-list podman \
		--non-interactive \
		--name podman_openstack \
		--url $CI_SERVER_URL \
		--registration-token $REGISTRATION_TOKEN \
		--executor custom \
		--builds-dir $RUNNER_DATA/ci/builds \
		--cache-dir $RUNNER_DATA/ci/cache  \
		--custom-config-exec $BINDIR/ci-podman-config.sh \
		--custom-config-exec-timeout 3 \
		--custom-prepare-exec $BINDIR/ci-podman-prepare.sh \
		--custom-prepare-exec-timeout $(( 30 * 60 ))            \
		--custom-run-exec $BINDIR/ci-podman-run.sh \
		--custom-cleanup-exec $BINDIR/ci-podman-cleanup.sh \
		--custom-cleanup-exec-timeout $(( 5 * 60 ))  \
		--custom-graceful-kill-timeout $(( 1 * 60 )) \
		--custom-force-kill-timeout $(( 3 * 60 ))
	echo "Done."
else
	echo "missing RUNNER_DATA or REGISTRATION_TOKEN"
	echo "RUNNER_DATA="$RUNNER_DATA
	echo "REGISTRATION_TOKEN="$REGISTRATION_TOKEN
fi

